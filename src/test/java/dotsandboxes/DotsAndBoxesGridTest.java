package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    // FIXME: You need to write tests for the two known bugs in the code.
    @Test
    public void testTestCompleteBox(){
        logger.info("Tests the algorithm for whether a box is complete");
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(10, 10, 2);

        //Create TestBoxes
        grid.drawHorizontal(0,0,1);
        grid.drawHorizontal(0,1,1);
        grid.drawHorizontal(0,2,2);
        grid.drawHorizontal(0,3,2);
        grid.drawHorizontal(1,0,1);
        grid.drawHorizontal(1,1,1);
        grid.drawHorizontal(1,3,2);
        grid.drawHorizontal(2,0,1);
        grid.drawHorizontal(2,1,2);
        grid.drawHorizontal(2,2,2);
        grid.drawHorizontal(2,3,2);
        grid.drawVertical(0,0,1);
        grid.drawVertical(0,1,1);
        grid.drawVertical(1,0,1);
        grid.drawVertical(1,1,1);
        grid.drawVertical(1,2,2);
        grid.drawVertical(2,0,2);
        grid.drawVertical(2,1,1);
        grid.drawVertical(2,2,2);
        grid.drawVertical(2,3,2);
        grid.drawVertical(3,1,2);
        grid.drawVertical(3,0,2);

        //Outbounds
        assertFalse(grid.boxComplete(0,-1));
        assertFalse(grid.boxComplete(0,10));
        assertFalse(grid.boxComplete(-1,0));
        assertFalse(grid.boxComplete(10,0));

        //Complete Box
        assertTrue(grid.boxComplete(0,0));
        assertTrue(grid.boxComplete(1,0));// not working
        assertTrue(grid.boxComplete(2,0));
        assertTrue(grid.boxComplete(2,1));
        assertTrue(grid.boxComplete(0,1));
        assertFalse(grid.boxComplete(1,1));
        assertFalse(grid.boxComplete(1,2));
        assertFalse(grid.boxComplete(0,2));
        assertFalse(grid.boxComplete(1,2));
        assertFalse(grid.boxComplete(2,2));
        assertFalse(grid.boxComplete(0,3));
        assertFalse(grid.boxComplete(1,3));
        assertFalse(grid.boxComplete(2,3));
        assertFalse(grid.boxComplete(3,3));
    }

    @Test
    public void testTestExceptionOnLineDrawnTwice(){
        logger.info("Tests Exception is thrown when a line is drawn twice.");
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(10, 10, 2);
        grid.drawHorizontal(0,0,1);
        grid.drawHorizontal(0,8,1);
        grid.drawHorizontal(8,1,1);
        grid.drawHorizontal(8,0,1);
        assertThrows(IndexOutOfBoundsException.class, ()->grid.drawVertical(-1,1,1));
        assertThrows(IndexOutOfBoundsException.class, ()->grid.drawHorizontal(-1,8,1));
        assertThrows(IndexOutOfBoundsException.class, ()->grid.drawVertical(10,1,1));
        assertThrows(IndexOutOfBoundsException.class, ()->grid.drawHorizontal(0,10,1));
        assertThrows(IllegalArgumentException.class, ()->grid.drawHorizontal(0,0,2));
        assertThrows(IllegalArgumentException.class, ()->grid.drawHorizontal(0,0,1));
        assertThrows(IllegalArgumentException.class, ()->grid.drawHorizontal(0,8,2));
        assertThrows(IllegalArgumentException.class, ()->grid.drawHorizontal(0,8,1));
        assertThrows(IllegalArgumentException.class, ()->grid.drawHorizontal(8,1,2));
        assertThrows(IllegalArgumentException.class, ()->grid.drawHorizontal(8,1,1));
        assertThrows(IllegalArgumentException.class, ()->grid.drawHorizontal(8,1,2));
        assertThrows(IllegalArgumentException.class, ()->grid.drawHorizontal(8,1,1));
        grid.drawVertical(0,0,1);
        grid.drawVertical(0,8,1);
        grid.drawVertical(8,0,1);
        grid.drawVertical(8,1,1);
        assertThrows(IllegalArgumentException.class, ()->grid.drawVertical(0,0,2));
        assertThrows(IllegalArgumentException.class, ()->grid.drawVertical(0,0,1));
        assertThrows(IllegalArgumentException.class, ()->grid.drawVertical(0,8,2));
        assertThrows(IllegalArgumentException.class, ()->grid.drawVertical(0,8,1));
        assertThrows(IllegalArgumentException.class, ()->grid.drawVertical(8,1,2));
        assertThrows(IllegalArgumentException.class, ()->grid.drawVertical(8,1,1));
        assertThrows(IllegalArgumentException.class, ()->grid.drawVertical(8,1,2));
        assertThrows(IllegalArgumentException.class, ()->grid.drawVertical(8,1,1));
    }
}


